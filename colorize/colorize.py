#!/usr/bin/python

import sys
import re
import fileinput
from optparse import OptionParser

DEBUG = True

BLUE='\033[34m'
GREEN='\033[32m'
YELLOW='\033[33m'
RED='\033[31m'
MAGENTA='\033[35m'
ENDC='\033[m'

COLOR_HIGH = ["PSCCSR","SpchCCSR","SHOSuccess","PSRrcSucRate","HsAccess","CSIuSigSuc",
              "PSIuSigSuc","PSRabSucc","PSRrcSucc","PsDCHFACHRabSucc","RrcSuc","SpchAccess",
              "PSAccess","SpchIuSigSuc","SpchRabSuc","SpchRrcSuc","SpchRrcSucRate"]
              # LTE 
COLOR_HIGH_LTE=[ "Acc_AddedERabEstabSuccRate","Acc_InitialERabEstabSuccRate","Acc_InitialErabSetupSuccRate",
               "Acc_InitialUEContextEstabSuccRate","Acc_RrcConnSetupSuccRate","Acc_S1SigEstabSuccRate","Mob_HoExecSuccRate","Mob_HoPrepSuccRate"]

COLOR_LOW = ["HsDrop","PSAccFailRate","PSDrop","SpchDrop","CellFACHDrop","SpchAccFailRate"]
COLOR_LOW_LTE=["Ret_ERabDrop","Ret_ERabRetainability","Ret_ERabRetainabilityRate"]

def parse_pmx(text):
    lines = text.split("\n")
    tmp_dict = {}
    for line in lines:
        if "Object" in line:
            words = [x for x in  line.split(" ") if len(x)]
            loci = [line.index(word) for word in words]
            times = words[2:]
        if "UtranCell" in line:
            sline = line.split(" ")
            vline = [x for x in sline if len(x)]
            pkey = vline[0]+vline[1]
            mo = vline[0].ljust(loci[1]-loci[0])
            pm = vline[1].ljust(loci[2]-loci[1])
            for time,val in zip(times,vline[2:]):
                tmp_dict[pkey+time] = int(val)
            if vline[1] == "pmTotNoRrcConnectReqSuccess":
                vals = [color(int(v),tmp_dict.get(vline[0]+"pmTotNoRrcConnectReq"+t))+v.rjust(5)+ENDC for t,v in zip(times,vline[2:])]
                print mo+pm+'  '.join(vals)
            elif vline[1] == "pmCellDowntimeAuto":
                vals = [color2(int(v) == 0)+v.rjust(5)+ENDC for t,v in zip(times,vline[2:])]
                print mo+pm+'  '.join(vals)
            elif vline[1] == "pmCellDowntimeMan":
                vals = [color3(int(v) == 0)+v.rjust(5)+ENDC for t,v in zip(times,vline[2:])]
                print mo+pm+'  '.join(vals)
            elif vline[1] == "pmRrcConnEstabSucc":
                vals = [color(int(v),tmp_dict.get(vline[0]+"pmRrcConnEstabAtt"+t))+v.rjust(5)+ENDC for t,v in zip(times,vline[2:])]
                print mo+pm+'  '.join(vals)
            elif vline[1] == "pmErabEstabSuccInit":
                vals = [color(int(v),tmp_dict.get(vline[0]+"pmErabEstabAttInit"+t))+v.rjust(5)+ENDC for t,v in zip(times,vline[2:])]
                print mo+pm+'  '.join(vals)
            elif vline[1] == "pmNoRabEstablishSuccessSpeech":
                vals = [color(int(v),tmp_dict.get(vline[0]+"pmNoRabEstablishAttemptSpeech"+t))+v.rjust(5)+ENDC for t,v in zip(times,vline[2:])]
                print mo+pm+'  '.join(vals)
            elif vline[1] == "pmNoRabEstablishSuccessPacketInteractive":
                vals = [color(int(v),tmp_dict.get(vline[0]+"pmNoRabEstablishAttemptPacketInteractive"+t))+v.rjust(5)+ENDC for t,v in zip(times,vline[2:])]
                print mo+pm+'  '.join(vals)
            else:
                print line
        else:
            print line
    #print tmp_dict

def color(v1,v2):
    try:
        v1 = float(v1)
        v2 = float(v2)
        if v1 > .98*v2:
            return GREEN
        if v1 > .95*v2:
            return YELLOW
        return RED
    except:
        return MAGENTA

def color2(v):
    try:
        if v:
            return GREEN
        else:
            return RED
    except:
        return MAGENTA

def color3(v):
    try:
        if v:
            return GREEN
        else:
            return YELLOW
    except:
        return MAGENTA

def color_high(v,d=1.0):
    if v == "N/A": return ""
    try:
        v = float(v)/d
        if v > 98.0:
           return GREEN
        if v > 95.0:
            return YELLOW
    except:
        pass
    return RED

def color_low(v,d=1.0):
    if v == "N/A": return ""
    try:
        v = float(v)/d
        if v < 2.0:
            return GREEN
        if v < 5.0:
            return YELLOW
    except:
        pass
    return RED

def color_high_LTE(v,d=1.0,r=[99.0,98.0]):
    if v == "N/A": return ""
    try:
        v = float(v)/d
        if v >= r[0]: return GREEN
        #if v >= r[1]: return YELLOW
    except:
        pass
    return RED

def color_low_LTE(v,d=1.0):
    if v == "N/A": return ""
    try:
        v = float(v)/d
        if v <= 1.0: return GREEN
        #if v <= 2.0: return YELLOW
    except:
        pass
    return RED

def color_high_inline(v,normalize=100.0,ranges=(0.98,0.95)):
    try:
        tv = float(v)/normalize
        if tv > ranges[0]: return GREEN+v+ENDC
        elif tv > ranges[1]: return YELLOW+v+ENDC
        else: return RED+v+ENDC
    except:
        return MAGENTA+v+ENDC

def color_low_inline(v,normalize=100.0,ranges=(0.02,0.05)):
    try:
        tv = float(v)/normalize
        if tv < ranges[0]: return GREEN+v+ENDC
        elif tv < ranges[1]: return YELLOW+v+ENDC
        else: return RED+v+ENDC
    except:
        return MAGENTA+v+ENDC

def parse_pmr3(text):
    lines = text.split("\n")
    for line in lines:
        if len(line) > 5:
            words = [x for x in  line.split(" ") if len(x)]
            loci = [line.index(word) for word in words]
            pm = words[0]
            vals = [ v.rjust(6) for v in words[1:]]
            if pm in COLOR_HIGH:
                vals = [color_high(v)+v+ENDC for v in vals]
            elif pm in COLOR_LOW:
                vals = [color_low(v)+v+ENDC for v in vals]
            elif pm in COLOR_LOW_LTE:
                vals = [color_low_LTE(v)+v+ENDC for v in vals]
            elif pm in COLOR_HIGH_LTE:
                vals = [color_high_LTE(v)+v+ENDC for v in vals]
            print "       "+pm.ljust(21)+' '.join(vals)
            # todo: fix just. on lines to match input
            #print " "*(line.find(pm))+pm.ljust(21)+' '.join(vals)

def parse_pmr_performance(text,tag_filter=False):
    lines = text.split("\n")
    for line in lines:
        words = re.split(r"(\s+)",line)
        if len(words) > 1:
            #pm = [w for w in words if len(w) and " " not in w][0]
            pm = [w for w in words if len(w) and " " not in w and ":" not in w][0]
            new_line = ""
            for w in words:
                if len(w):
                    if w == pm:
                        new_line += pm
                    #elif " " in w:
                    elif " " in w or ":00" in w or ":15" in w or ":30" in w or ":45" in w:
                        new_line += w
                    elif pm == "Mob_HoExecSuccRate" and ":00" in new_line: # acceptable range for Mob_HoExecSuccRate depends on report run. this matches pmr -r 5. There should be a better way to do this.
                        new_line += color_high_LTE(w,r=[90.0,99.0])+w+ENDC
                    elif pm in COLOR_HIGH:
                        new_line += color_high(w)+w+ENDC
                    elif pm in COLOR_LOW:
                        new_line += color_low(w)+w+ENDC
                    elif pm in COLOR_HIGH_LTE:
                        new_line += color_high_LTE(w)+w+ENDC
                    elif pm in COLOR_LOW_LTE:
                        new_line += color_low_LTE(w)+w+ENDC
                    elif "Int_RadioRecInterference" in pm:
                        new_line += color2( -119 <= float(w) <= -117 )+w+ENDC
                    else:
                        new_line += w
            if not tag_filter or (tag_filter and "\033[" in new_line): print(new_line)      

def parse_pmr_utrancell(text):
    lines = text.split("\n")
    
    # find the longest MO name, used for padding below
    max_line_start = 0
    for line in lines:
        words = [x for x in  lines[0].split(" ") if len(x)]
        if len(words) > 2:
            line_start = len(" "+words[0]+" "+words[1]+" ")
            if line_start > max_line_start: max_line_start = line_start

    # pmr gives different spacing between the object and value columns
    # get the spacing starting after the string used to generate line_start
    # above. pad should match all but the first number if you did the same
    # operation on kpis below.     
    tmp_words = []
    tmp_word_parts = lines[0][line_start:].split(" ")[::-1]
    tmp_word = tmp_word_parts[0]
    for w in tmp_word_parts[1:]:
        if len(w):
            tmp_words.append(tmp_word)
            tmp_word = w
        else:
            tmp_word = " "+tmp_word
    tmp_words.append(tmp_word)
    pads = [len(x) for x in tmp_words[::-1]]

    # 14 RRC Performance
    kpis = "NoRrcConnReq NoRrcConnSuc RrcFail RrcFailContrib RrcSuc"
    if [len(x) for x in kpis.split(" ") if len(x) ][-kpis.count(" "):] == pads[1:]:
        for line in lines:
            words = [x for x in  line.split(" ") if len(x)]
            #pads = [len(x) for x in kpis.split(" ")]
            if len(words) == 7:
                new_line = " "+words[0]+" "+words[1]
                new_line = new_line.ljust(max_line_start)
                wds = words[2:]
                new_line += color_low(wds[2])+wds[0].rjust(pads[0])+ENDC+" "
                new_line += color_low(wds[2])+wds[1].rjust(pads[1])+ENDC+" "
                new_line += color_low(wds[2])+wds[2].rjust(pads[2])+ENDC+" "
                new_line += color_low(wds[3],10.0)+wds[3].rjust(pads[3])+ENDC+" "
                new_line += color_high(wds[4])+wds[4].rjust(pads[4])+ENDC
                print new_line
        return
    #15 
    kpis = "SpchAccFailRate SpchAccess SpchCCSR SpchDrop SpchDropContrib SpchNoRabEstAtt SpchRabSuc"
    #if len(lines[0][line_start:]) == len(kpis) or (len(lines[0][line_start:]) == len(kpis)+2 and len([x for x in  lines[0].split(" ") if len(x)]) ==9):
    if [len(x) for x in kpis.split(" ") if len(x) ][-kpis.count(" "):] == pads[1:]:
        for line in lines:
            words = [x for x in  line.split(" ") if len(x)]
            pads = [len(x) for x in kpis.split(" ")]
            if len(words) == 9:
                new_line = " "+words[0]+" "+words[1]+" "
                new_line = new_line.ljust(max_line_start)
                wds = words[2:]
                new_line += color_low(wds[0])+wds[0].rjust(pads[0])+ENDC+" "
                new_line += color_high(wds[1])+wds[1].rjust(pads[1])+ENDC+" "
                new_line += color_high(wds[2])+wds[2].rjust(pads[2])+ENDC+" "
                new_line += color_low(wds[3])+wds[3].rjust(pads[3])+ENDC+" "
                new_line += color_low(wds[4],10.0)+wds[4].rjust(pads[4])+ENDC+" "
                new_line += wds[5].rjust(pads[5])+" "
                new_line += color_high(wds[6])+wds[6].rjust(pads[6])+ENDC
                print new_line
        return
    #17
    kpis = "CellFACHDrop DCHMinperDrop FACHMinperDrop PSAccFailRate PSAccess PSCCSR PSDrop PSDropContrib PSNoRabEstAtt PSRabSucc"
    if [len(x) for x in kpis.split(" ") if len(x) ][-kpis.count(" "):] == pads[1:]:
    #if len(lines[0][line_start:]) == len(kpis) or (len(lines[0][line_start:]) == len(kpis)+2 and len([x for x in  lines[0].split(" ") if len(x)]) ==12):
        for line in lines:
            words = [x for x in  line.split(" ") if len(x)]
            pads = [len(x) for x in kpis.split(" ")]
            if len(words) == 12:
                new_line = " "+words[0]+" "+words[1]+" "
                new_line = new_line.ljust(max_line_start)
                wds = words[2:]
                new_line += color_low(wds[0])+wds[0].rjust(pads[0])+ENDC+" "
                new_line += wds[1].rjust(pads[1])+ENDC+" "
                new_line += wds[2].rjust(pads[2])+ENDC+" "
                new_line += color_low(wds[3])+wds[3].rjust(pads[3])+ENDC+" "
                new_line += color_high(wds[4])+wds[4].rjust(pads[4])+ENDC+" "
                new_line += color_high(wds[5])+wds[5].rjust(pads[5])+ENDC+" "
                new_line += color_low(wds[6])+wds[6].rjust(pads[6])+ENDC+" "
                new_line += color_low(wds[7],10)+wds[7].rjust(pads[7])+ENDC+" "
                new_line += wds[8].rjust(pads[8])+" "
                new_line += color_high(wds[9])+wds[9].rjust(pads[9])+ENDC
                print new_line
        return
    #19
    kpis = "EULMinperDrop HSMinperDrop HsAccess HsAvgNoUsers HsCellChAttempt HsCellChSuccRate HsDrop HsDropContrib NoAbnormalHsRel NoHsRel"
    if [len(x) for x in kpis.split(" ") if len(x) ][-kpis.count(" "):] == pads[1:]:
    #if len(lines[0][line_start:]) == len(kpis) or (len(lines[0][line_start:]) == len(kpis)+2 and len([x for x in  lines[0].split(" ") if len(x)]) ==12):
        for line in lines:
            words = [x for x in  line.split(" ") if len(x)]
            pads = [len(x) for x in kpis.split(" ")]
            if len(words) == 12:
                new_line = " "+words[0]+" "+words[1]+" "
                new_line = new_line.ljust(max_line_start)
                wds = words[2:]
                new_line += wds[0].rjust(pads[0])+" "
                new_line += wds[1].rjust(pads[1])+" "
                new_line += color_high(wds[2])+wds[2].rjust(pads[2])+ENDC+" "
                new_line += wds[3].rjust(pads[3])+" "
                new_line += wds[4].rjust(pads[4])+" "
                new_line += color_high(wds[5])+wds[5].rjust(pads[5])+ENDC+" "
                new_line += color_low(wds[6])+wds[6].rjust(pads[6])+ENDC+" "
                new_line += color_low(wds[7],10)+wds[7].rjust(pads[7])+ENDC+" "
                new_line += wds[8].rjust(pads[8])+" "
                new_line += wds[9].rjust(pads[9])
                print new_line
        return
    print MAGENTA+text+ENDC

def parse_pmr_utrancell2(text):
    lines = text.split("\n")
    max_line_start = 0
    for line in lines:
        words = [x for x in  lines[0].split(" ") if len(x)]
        if len(words) > 2:
            line_start = len(" "+words[0]+" "+words[1]+" ")
            if line_start > max_line_start: max_line_start = line_start

def isfloat(x):
    try:
        a = float(x)
    except ValueError:
        return False
    else:
        return True

def isint(x):
    try:
        a = float(x)
        b = int(a)
    except ValueError:
        return False
    else:
        return a == b

def parse_pmr_by_field_width(text,tag_filter,tag_debug):
    lines = [line for line in text.split("\n") if len(line)]
    all_lines = []
    for line in lines:
        fields = [w for w in re.split(r"(\s+[^\s]+\b)",line) if len(w)]
        field_widths = map(len,fields)
        if field_widths[1:] == [8, 9, 16, 21, 12]:
            #  1) Carrier Average Rssi and Transmitted Power, Whole Period
            all_lines.append(''.join((fields[0:1]+[color2( -108.0 <= float(fields[1]) <= -98.0 )+(fields[1])+ENDC]+fields[2:])))
        elif field_widths[1:] == [8, 12,9, 16, 21, 12]:
            #  1) Carrier Average Rssi and Transmitted Power, Whole Period
            # another version
            all_lines.append(''.join((fields[0:1]+[color2( -108.0 <= float(fields[1]) <= -98.0 )+(fields[1])+ENDC,color2( -108.0 <= float(fields[2]) <= -98.0 )+(fields[2])+ENDC]+fields[3:])))
        elif field_widths[-4:] == [10, 11, 11, 15]:
            # rnc> pmr -r 25
            all_lines.append(''.join(fields[:-1]) + color_high(fields[-1])+fields[-1]+ENDC)
        elif field_widths[-6:] == [16, 17, 11, 11, 11, 15] or field_widths[-6:] == [ 14, 15, 10, 9, 9, 13]:
            # rnc> pmr -r 27,30
            all_lines.append(''.join(fields[:-7]) + \
            (color_low(fields[-7])+fields[-7]+ENDC) + \
            (fields[-6]) + \
            (fields[-5]) + \
            (color_high(fields[-4])+fields[-4]+ENDC) + \
            (fields[-3]+ENDC) + \
            (fields[-2]+ENDC) + \
            (color_high(fields[-1])+fields[-1]+ENDC) \
            )
        elif field_widths[-5:] == [ 9, 9, 16, 16, 11]:
            # rnc> pmr -r 31,37
            all_lines.append(''.join(fields[:-6]) + \
            (color_high(fields[-6])+fields[-6]+ENDC) + \
            (color_high(fields[-5])+fields[-5]+ENDC) + \
            (color_low(fields[-4])+fields[-4]+ENDC) + \
            (color_low(fields[-3])+fields[-3]+ENDC) + \
            (fields[-2]+ENDC) + \
            (color_high(fields[-1])+fields[-1]+ENDC) \
            )
        elif field_widths[-6:] == [14, 15, 9, 7, 7, 14]:
            # rnc> pmr -r 32,39
            all_lines.append(''.join(fields[:-7]) + \
            (fields[-7]+ENDC) + \
            (fields[-6]+ENDC) + \
            (fields[-5]+ENDC) + \
            (color_high(fields[-4])+fields[-4]+ENDC) + \
            (color_high(fields[-3])+fields[-3]+ENDC) + \
            (color_low(fields[-2])+fields[-2]+ENDC) + \
            (color_low(fields[-1])+fields[-1]+ENDC) \
            )
        elif field_widths[-5:] == [13,13,8,15,7]:
            # rnc> pmr -r 14
            all_lines.append(''.join(fields[:-7]) + \
            (fields[-7]+ENDC) + \
            (fields[-6]+ENDC) + \
            (color_low(fields[-3])+fields[-5]+ENDC) + \
            (color_low(fields[-3])+fields[-4]+ENDC) + \
            (color_low(fields[-3])+fields[-3]+ENDC) + \
            (color_low(fields[-2])+fields[-2]+ENDC) + \
            (color_low(fields[-3])+fields[-1]+ENDC) \
            )
        elif len(field_widths) == 13 and field_widths[-10:] == [13, 14, 15, 14, 9, 7, 7, 14, 14, 10]:
            # RNC> pmr -r 17
            if tag_debug: print("assuming `pmr -r 17`"+ENDC+str(fields)+str(field_widths))
            all_lines.append(''.join(fields[:-10]) + \
            color_low_inline(fields[-10]) + fields[-9] + fields[-8] +\
            color_low_inline(fields[-7]) + \
            color_high_inline(fields[-6]) + color_high_inline(fields[-5]) + \
            color_low_inline(fields[-4]) + color_low_inline(fields[-3],ranges=(1.0/len(lines),2.0/len(lines))) + \
            fields[-2] + color_high_inline(fields[-1]) )
        else:
            all_lines.append(MAGENTA + ''.join(fields) + ENDC + ((" "+str(field_widths)) if tag_debug else "" ))
    return all_lines

def main(argv):

    parser = OptionParser()
    parser.add_option("-f","--filter",dest="filter",help="filter out non-colored lines",default=False,action="store_true")
    parser.add_option("--debug",dest="debug",help="enable ",default=False,action="store_true")

    options,files = parser.parse_args(argv)
    text = "".join([line for line in fileinput.input(files)])
    if sum([pm in text for pm in ["pmTotNoRrcConnectReq","pmCellDowntime","pmNoRabEstablishAttempt"]]):
        parse_pmx(text)
#    elif "AvgSpeechErlang" in text:
    elif "SpchAccess" in text:
        parse_pmr_performance(text)
    elif "Acc_InitialUEContextEstabSuccRate" in text or "Int_RadioRecInterference" in text:
        parse_pmr_performance(text,options.filter)
#    elif "Proxy  Adm State     Op. State     MO" in text:
#        parse_st(text)
    elif "UtranCell=" in text or " Sector=" in text or " Iub_" in text:
        print "\n".join(parse_pmr_by_field_width(text,False,options.debug))
    elif "EUtranCellFDD=" in text:
        parse_pmx(text)
    else:
        print(MAGENTA+text+ENDC+"\n"+RED+"unsupported input"+ENDC)

if __name__ == '__main__':
   main(sys.argv[1:])

