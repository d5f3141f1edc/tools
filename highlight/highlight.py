#!/usr/bin/python

from optparse import OptionParser
from itertools import chain, izip
import sys
import re


BLUE='\033[34m'
GREEN='\033[32m'
YELLOW='\033[33m'
RED='\033[31m'
MAGENTA='\033[35m'
ENDC='\033[m'

def main():
    """
    text search and hilighting for when you only have access to an old version of grep
    """
    parser = OptionParser(usage="usage %prog [options]",version="%prog 20170526")
    parser.add_option("-i",dest="case_insensitive",action="store_true",help="ignore case when matching",default=False)
    parser.add_option("-f","--filter",dest="filter",action="store_true",help="",default=False)
    parser.add_option("-r","--red",dest="red",help="highlight matching lines in red",metavar="regex")
    parser.add_option("-g","--green",dest="green",help="highlight matching lines in green",metavar="regex")
    parser.add_option("-b","--blue",dest="blue",help="highlight matching lines in blue",metavar="regex")
    parser.add_option("-y","--yellow",dest="yellow",help="highlight matching lines in yellow",metavar="regex")
    (options,arg)=parser.parse_args()

    # create a list of complied regex and color code tuples to be looped through later
    # more than only match can happen per line, but matches can not overlap
    regexs = []
    if options.red: regexs.append((re.compile(options.red,re.I if options.case_insensitive else 0),RED))
    if options.green: regexs.append((re.compile(options.green,re.I if options.case_insensitive else 0),GREEN))
    if options.blue: regexs.append((re.compile(options.blue,re.I if options.case_insensitive else 0),BLUE))
    if options.yellow: regexs.append((re.compile(options.yellow,re.I if options.case_insensitive else 0),YELLOW))
    #if not options.filter: regexs.append((re.compile(".*"),ENDC)) # match and print any line without highlighting

    if not sys.stdin.isatty():
        for line in sys.stdin:
            line = line.rstrip("\r\n")
            line_ = ''
            for reg,color in regexs:
                if reg.search(line):
                    re_split = reg.split(line)
                    re_match = map(lambda t: color+t+ENDC,reg.findall(line))+['']
                    line = "".join(chain.from_iterable(izip(re_split,re_match)))
            # only show line if filtering is disabled, or if fitlering is on and a color code is present
            if not options.filter or '\033[' in line: print(line)

if __name__ == '__main__':
    main()
