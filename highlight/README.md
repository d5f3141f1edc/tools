# highlight

Highlight lines for stdin based on regular expressions. Useful for older systems without a modern version of grep.
