#!/usr/bin/python

import sys
import pickle
import imagehash
import os
import argparse
from collections import namedtuple
import Image

hash_class = namedtuple('hashTuple',['path','str_hash','str_hash_90','str_hash_180','str_hash_270','size','timestamp','filesize'])
hash_result = namedtuple('hashResult',['isref','distance','hashTuple'])

parser = argparse.ArgumentParser(description="Find and remove duplicate images")
parser.add_argument("-v",help="verbose output",action="store_true")
parser.add_argument("-r",help="recersively traverse directories",action="store_true")
parser.add_argument("-s","--size",help="size of image hash to use", type=int, default=8)
parser.add_argument("-t","--type",help="type of image hash to use", choices=['average','perception','difference'],default='average')
parser.add_argument("-N","--noprompt",help="delete duplicates without asking user",action="store_true")
parser.add_argument("--ref",help="referance directory, file will be searched, but not removed")
parser.add_argument("dir",nargs='+',help="path(s) to search")
args = parser.parse_args()

def do_hash(filename):
    try:
        i = Image.open(filename)
        size = (128,128)
	i.thumbnail( size )
        if args.type == "average":
            hfunc = imagehash.average_hash
        elif args.type == "perception":
            hfunc = imagehash.phash
        elif args.type == "difference":
            hfunc = imagehash.dhash
        h = hfunc(i,args.size)
	h90 = hfunc(i.rotate(90),args.size)
        h180 = hfunc(i.rotate(180),args.size)
        h270 = hfunc(i.rotate(270),args.size)
        return hash_class(filename,str(h),str(h90),str(h180),str(h270),i.size[0]*i.size[1],os.path.getctime(filename),os.path.getsize(filename))
    except IOError:
        pass
    print("failed to get hash on {}".format(filename))
    return 

def main():
    print("Building file list...")
    file_set = set([file for file in args.dir if os.path.isfile(file)])

    ref_set = set([])
    if args.ref and os.path.isdir(args.ref):
        for dirpath, dirnames, filenames in os.walk(args.ref):
            ref_set.update(set([os.path.join(dirpath,f) for f in filenames]))

    if args.r:
        for dir in [dir for dir in args.dir if os.path.isdir(dir)]:
            for dirpath, dirnames, filenames in os.walk(dir):
                file_set.update(set([os.path.join(dirpath,f) for f in filenames]))

    file_set -= ref_set

    print("Found {} files{}.".format(len(file_set),", {} more used as referances".format(len(ref_set)) if len(ref_set) else "" ))

    print("Calculating hashes...")
    ref_hash = filter( lambda a: a is not None,map(do_hash,ref_set))
    file_hash = filter(lambda a: a is not None,map(do_hash,file_set))

    sizes = []

    for hash in file_hash:
        suspect_list = [hash_result(isref=False,distance=0,hashTuple=hash)]
        for hash2 in file_hash:
            if hash.path != hash2.path and (hash.str_hash == hash2.str_hash or hash.str_hash == hash2.str_hash_90 or hash.str_hash == hash2.str_hash_180 or hash.str_hash == hash2.str_hash_270):
                suspect_list.append(hash_result(isref=False,distance=0,hashTuple=hash2))
        for hash3 in ref_hash:
            if hash.str_hash == hash3.str_hash:
                suspect_list.append(hash_result(isref=True,distance=0,hashTuple=hash3))
        if len(suspect_list) > 1:
            print("")
            for i,h in enumerate(sorted(suspect_list,key=lambda k: (not k.isref,k.distance,k.hashTuple.size,k.hashTuple.timestamp))):
                if i and not h.isref:
                    print("[-] {}".format(h.hashTuple.path))
                    # delete file
                    if args.noprompt:
                        #print("rm {}".format(h.hashTuple.path)) 
                        os.remove(h.hashTuple.path)
                    sizes.append(h.hashTuple.filesize)
                    # remove hash from list
                    file_hash.remove(h.hashTuple)
                else:
                    print("[+] {}".format(h.hashTuple.path))
                    

    print("Deleted {} file{}, freeing {} {}{}".format(len(sizes),"s" if len(sizes) else "", sum(sizes),"byte","s" if sum(sizes) else "" ))

if __name__ == '__main__':
    main()
