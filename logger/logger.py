#!/usr/bin/python3

from optparse import OptionParser
from itertools import count
from datetime import datetime,timedelta
import os
import sys
import re
import time

def main():
    parser = OptionParser()
    parser.add_option('-f','--file',dest="file_prefix",help="log file prefix",default=None)
    parser.add_option('-s','--size',dest="max_filesize",type='int',help='maximum file size in bytes',default=0)
    #parser.add_option('-h','--header',dest="carry_header",type='bool', \
    #help='put a copy of the header in each new file. The first line of input is ' \
    #'assumed to be the header.', default=False) 
    parser.add_option('--debug',dest='debug',default=False,action='store_true',help='turn on debug messages')
    (options,arg) = parser.parse_args()
    
    today,next_rotate_time,paths = step_date(options.file_prefix,datetime.today()-timedelta(days=1))
    fout = acquire_log(paths)
    if options.debug: print("{}-DEBUG: logging to {}".format(time.time(),fout.name))

    for line in sys.stdin:
        if next_rotate_time <= time.time():
            today,next_rotate_time,paths = step_date(options.file_prefix,today)
            fout.close()
            fout = acquire_log(paths)
            if options.debug: print("{}-DEBUG: logging to {}".format(time.time(),fout.name))
        bytes = line.encode('utf-8')
        # rotate log iff atleast 1 write has occured, max_filesize is set and writing the next line would put 
        # you over the max_filesize
        if fout.tell() and options.max_filesize and fout.tell()+len(bytes) > options.max_filesize:
            fout.close()
            fout = acquire_log(paths)
            if options.debug: print("{}-DEBUG: logging to {}".format(time.time(),fout.name))
        fout.write(bytes)

def step_date(file_prefix,yesterday):
    today = datetime(yesterday.year,yesterday.month,yesterday.day,0,0,0) \
          + timedelta(days=1)
    next_rotate_time = int((today+timedelta(days=1)).strftime("%s"))
    paths = log_paths(file_prefix,today.strftime("%Y%m%d"))
    return (today,next_rotate_time,paths)

def acquire_log(paths):
    '''return file handle for the next log file in paths'''
    for path in paths:
        try:
            return open(path,'xb')
        # if file exists, try the next path
        except FileExistsError:
            pass
    # if you magically run out of paths error out
    raise FileNotFoundError

def log_paths(prefix,postfix):
    '''return generator for file paths of form prefile-yyyymmdd[-nnn]'''
    for i in count(0):
        yield("{}-{}{}".format( \
            prefix, \
            postfix, \
            ('-{}'.format(str(i).zfill(3)) if i else '') \
            ) \
        )

if __name__ == '__main__':
    main()

