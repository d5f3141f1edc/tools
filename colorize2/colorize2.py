#!/usr/bin/python
import fileinput
import sys
import logging
from optparse import OptionParser
from itertools import izip_longest
from collections import defaultdict

BLUE='\033[34m'
GREEN='\033[32m'
YELLOW='\033[33m'
RED='\033[31m'
MAGENTA='\033[35m'
ENDC='\033[m'

root = logging.getLogger()
root.setLevel(logging.DEBUG)

log_handler = logging.StreamHandler(sys.stdout)
log_handler.setLevel(logging.DEBUG)
log_format = logging.Formatter('%(name)s:%(levelname)s:%(message)s')
log_handler.setFormatter(log_format)
root.addHandler(log_handler)


def pmx_to_dict(pmx):
    """
    convert the output of pmx command to a nested dict of the form
    {object:{counter:{rop:value,rop2:value,...},...},...}
    """
    results = defaultdict(lambda : defaultdict(lambda : defaultdict(str)))
    #all_rops = set()
    for line in (line.strip() for line in pmx if len(line) > 1):
        words = [word for word in line.split(' ') if len(word)]
        try:
            if words[0] == 'Date:':
                date = words[1]
            elif words[0] == 'Object' and words[1] == 'Counter':
                rops = words[2:]
                fixed_spacing = line.find(rops[1])-line.find(rops[0])
            elif len(words) > 2:
                object = words[0]
                counter = words[1]
                # split based on whitespace won't work if counters are empty for a rop
                # but fixed witdth won't work for sequence based coutners.
                if len(words[2:]) == len(rops):
                    # whitespace based splitting
                    values = words[2:]
                elif len(words[1:]) == len(rops):
                    #print(words,rops)
                    values = words[1:]
                    counter = words[0]
                    object = ''
                else:
                    # fixed width splitting
                    reversed_line = line[::-1]
                    reversed_words = [''.join(reversed(reversed_line[i:i+fixed_spacing])) for i in range(0,len(reversed_line),fixed_spacing)]
                    values = [word.strip(" ") for word in reversed_words[:len(rops)]][::-1]

                for rop,value in zip(rops,values):
                    #all_rops.update(date+'_'+rop)
                    # decompose sequences
                    if value.count(','):
                        for sequence_index,sequence_value in enumerate(value.split(','),1):
                            results[object][counter+'_'+str(sequence_index)][date+' '+rop] = sequence_value
                    else:
                        results[object][counter][date+' '+rop] = value
                    # fixed width splitting
        except Exception as e:
            logging.debug(e)
            logging.info('no useful information from \'%s\'' % (line,))

    return results
    #return dict(((k,dict(v)) for k,v in results.iteritems()))

def dict_to_text(d):
    """
    Print dict as close to pmr as possible
    """
    # build sets of all keys...
    all_objects = set()
    all_counters = set()
    all_rops = set()
    max_len_value = 6
    for object,counters in d.iteritems():
        for pm,rops in counters.iteritems():
            all_rops.update(rops.keys())
            for rop,value in rops.iteritems():
                #print rop,len(rop)
                max_len_value = max(max_len_value,len(value))
        all_counters.update(counters.keys())
    # and store as sorted lists, they will be looped over a few times
    all_objects.update(d.keys())
    all_objects = sorted(all_objects)
    all_counters = sorted(all_counters)
    all_rops = sorted(all_rops)
    pads = [ max([len('Object')]+[len(object) for object in all_objects])+1,max([len(counter) for counter in all_counters])]+[max_len_value+1]*len(all_rops)
    headers = ['Object'.ljust(pads[0],' '),'Counter'.ljust(pads[1])] + [ t.rjust(p,' ') for p,t in zip(pads[2:],[rop[-5:] for rop in all_rops])]
    lines = [''.join(headers)]
    for object in all_objects:
        for counter in all_counters:
            if d[object][counter]:
                line_start = object.ljust(pads[0],' ')+counter.ljust(pads[1],' ')
                #line_values = ''.join([ value.rjust(pad,' ') for pad,value in zip(pads[2:],[d[object][counter][rop] for rop in all_rops])])
                line_values = []
                for pad,rop in zip(pads[2:],all_rops):
                    value = d[object][counter][rop]
                    color_code = color(value,d,object,counter,rop)
                    line_values.append(''.join((color_code,value.rjust(pad,' '),ENDC)))
                lines.append(''.join((line_start,''.join(line_values))))
    return "\n".join(lines)


def color(value,d,object,counter,rop):
    """Return the color code to be applied to value according to the configuration below"""
    kpi_single_color = {
        # LTE
        ('pmCellDowntimeAuto',):{'type':int,'coloring':'red-high','limits':(1,.5,-1)},
        ('pmCellDowntimeMan',):{'type':int,'coloring':'red-high','limits':(9000,1,-1)},
        # LTE PMR103
        ('Acc_AddedERabEstabSuccRate','Acc_InitialERabEstabSuccRate','Acc_InitialErabSetupSuccRate','Acc_InitialUEContextEstabSuccRate','Acc_RrcConnSetupSuccRate','Acc_S1SigEstabSuccRate','Mob_HoExecSuccRate','Mob_MobilitySuccRate'):{'type':float,'coloring':'green-high','limits':(99,97,-1)},
        ('Ret_ERabDrop',):{'type':float,'coloring':'red-high','limits':(3,1,-1)},
        # UMTS
        # RNC - pmr3
        ('CSIuSigSuc','HsAccess','PSAccess','PSCCSR','PSIuSigSuc','PSRabSucc','PSRrcSucRate','RrcSuc','SHOSuccess','SpchAccess','SpchCCSR','SpchRabSuc','SpchRrcSucRate'):{'type':float,'coloring':'green-high','limits':(99,98,-1)},
        ('CellFACHDrop','HsDrop','PSDrop','SpchAccFailRate','SpchDrop'):{'type':float,'coloring':'red-high','limits':(3,1.5,-1)},
    }

    kpi_paired = {
        # LTE
        ('pmErabEstabSuccInit','pmErabEstabAttInit'):{'type':float,'coloring':'green-high','limits':(0.98,0.95,-1)},
        ('pmRrcConnEstabSucc','pmRrcConnEstabAtt'):{'type':float,'coloring':'green-high','limits':(0.98,0.95,-1)},
        ('pmS1SigConnEstabSucc','pmS1SigConnEstabAtt'):{'type':float,'coloring':'green-high','limits':(0.98,0.95,-1)},
        ('pmErabEstabSuccAddedQci','pmErabEstabAttAddedQci'):{'type':float,'coloring':'green-high','limits':(0.98,0.95,-1)},
        # LTE PMR-103
        #():{'type':float,'coloring':'green-high','limits':()},
        # UMTS
        ('pmTotNoRrcConnectReqSuccess','pmTotNoRrcConnectReq'):{'type':float,'coloring':'green-high','limits':(0.98,0.95,-1)},
        ('pmNoRabEstablishSuccessPacketInteractive','pmNoRabEstablishAttemptPacketInteractive'):{'type':float,'coloring':'green-high','limits':(0.98,0.95,-1)},
        ('pmNoRabEstablishSuccessSpeech','pmNoRabEstablishAttemptSpeech'):{'type':float,'coloring':'green-high','limits':(0.98,0.95,-1)},
        # RNC - pmr3
        ('NoRrcConnSuc','NoRrcConnReq'):{'type':float,'coloring':'green-high','limits':(98,95,-1)},
    }

    try:
        config = {}
        #if counter in kpi_single_color.keys():
        for kpis in kpi_single_color.keys():
            if counter in kpis:
                config = kpi_single_color.get(kpis)
                cvalue = config.get('type')(value)
                break
        for a,b in kpi_paired.keys():
            if counter.startswith(a): # or counter == b:
                config = kpi_paired.get((a,b))
                suffix = counter.replace(a,"")
                n = config.get('type')(d[object][a+suffix][rop])
                d = config.get('type')(d[object][b+suffix][rop])
                try:
                    cvalue = n/d
                except ZeroDivisionError:
                    cvalue = 0
                break
            elif counter.startswith(b):
                return ENDC
        if config.get('coloring') ==  'red-high':
            cvalue = config.get('type')(value)
            high,med,low = config.get('limits')
            #print(cvalue,high,med,low)
            if cvalue >= high: return RED
            elif cvalue >= med: return YELLOW
            elif cvalue >= low: return GREEN
        if config.get('coloring') ==  'green-high':
            cvalue = config.get('type')(value)
            high,med,low = config.get('limits')
            #print(cvalue,high,med,low)
            if cvalue >= high: return GREEN
            elif cvalue >= med: return YELLOW
            elif cvalue >= low: return RED
    except Exception as e:
        logging.debug(e)
        return MAGENTA
    return ''

def main(argv):
    parser = OptionParser()
    options,files = parser.parse_args(argv)
    print( dict_to_text(pmx_to_dict(fileinput.input(files))))

if __name__ == '__main__':
    main(sys.argv[1:])

